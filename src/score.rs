use diesel;
use diesel::prelude::*;
use diesel::mysql::MysqlConnection;
use diesel::query_builder::AsChangeset;

use image_base64;

use crate::schema::pong_score;

#[table_name = "pong_score"]
#[derive(Serialize, Deserialize, Queryable, Insertable, AsChangeset)]
pub struct Score {
    pub id: i32,
    pub left_score: Option<i32>,
    pub right_score: Option<i32>,
    pub goal_sshot: String,
}

impl Score {
    pub fn create(mut score: Score, connection: &MysqlConnection) -> Score {
        // Screenshot png encoding to base64.
        score.goal_sshot = image_base64::to_base64("/home/kindacommander/Development/Rust/Rocket/pong_api/screenshot.png");

        diesel::insert_into(pong_score::table)
            .values(&score)
            .execute(connection)
            .expect("Error creating new score");

        pong_score::table.order(pong_score::id.desc()).first(connection).unwrap()
    }

    pub fn read(connection: &MysqlConnection) -> Vec<Score> {
        pong_score::table.order(pong_score::id.asc()).load::<Score>(connection).unwrap()
    }

    pub fn update(mut score: Score, connection: &MysqlConnection) -> bool {
        // Screenshot png encoding to base64.
        score.goal_sshot = image_base64::to_base64("/home/kindacommander/Development/Rust/Rocket/pong_api/screenshot.png");

        diesel::update(pong_score::table.find(0))
            .set(&score)
            .execute(connection)
            .is_ok()
    }

    pub fn delete(connection: &MysqlConnection) -> bool {
        diesel::delete(pong_score::table.find(0))
            .execute(connection)
            .is_ok()
    }
}