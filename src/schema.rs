table! {
    pong_score (id) {
        id -> Integer,
        left_score -> Nullable<Integer>,
        right_score -> Nullable<Integer>,
        goal_sshot -> Text,
    }
}
