#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate std;
#[macro_use] extern crate rocket;
#[macro_use] extern crate diesel;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;

mod score;
mod schema;
mod db;

use score::Score;

use rocket_contrib::json::{Json, JsonValue};

use r2d2;
use r2d2_diesel;

#[get("/")]
pub fn read(connection: db::Connection) -> Json<JsonValue> {
    Json(json!(Score::read(&connection)))
}

#[put("/", data = "<score>")]
fn update(score: Json<Score>, connection: db::Connection) -> Json<JsonValue> {
    let update = Score { id: 0, ..score.into_inner() };
    Json(json!({
        "success": Score::update(update, &connection)
    }))
}

#[delete("/")]
fn delete(connection: db::Connection) -> Json<JsonValue> {
    Json(json!({
        "success": Score::delete(&connection)
    }))
}

fn main() {
    rocket::ignite()
        .mount("/write", routes![update, delete])
        .mount("/read", routes![read])
        .manage(db::connect())
        .launch();
}
