# CRUD web api for pong_notifier project

## Before running
This CRUD web api is developed using [Rocket.rs](https://rocket.rs/) web framework. It makes abundant use of Rust's syntax extensions and other advanced, unstable features. Because of this, we'll need to use a nightly version of Rust. This way, you need to configure Rust nightly as your default toolchain. You can make it global by running the command:
```bash
rustup default nightly
```
or if you prefer, you can use per-directory overrides to use the nightly version only for your Rocket project by running the following command in the directory:
```bash
rustup override set nightly
```

## How to run
```bash
sudo ROCKET_ENV=staging cargo run 
```

## License
Licensed under [MIT license](./licenses/LICENSE-MIT)
