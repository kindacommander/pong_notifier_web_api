-- Your SQL goes here
CREATE TABLE pong_score (
    id INT(11) NOT NULL PRIMARY KEY,
    left_score INT(11),
    right_score INT(11),
    goal_sshot TEXT NOT NULL
);

INSERT INTO pong_score (id, left_score, right_score, goal_sshot) VALUES (0, 0, 0, "not_yet_encoded");